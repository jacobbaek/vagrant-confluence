## What is purpose?
Install Confluence using Vagrant

## Environment
- Provider : KVM(tested ubuntu 19.04), Virtualbox(tested windows 10, ubuntu 19.04)
- Provisioner : Ansible
- Box : Centos/7

## Keep to know
- If you use vagrant on Windows, you should install vagrant-guest_ansible plugin
  - https://github.com/vovimayhem/vagrant-guest_ansible
  run the command before you run the "vagrant up"
  vagrant plugin install vagrant-guest_ansible; vagrant up

## How to use
Run "vagrant up" in the directory which is with Vagrantfile
After done, You can see the Confluence first setup page at the http://localhost:8025.

If you wanna own database setup, you can setup with MySQL DB.
This playbook already has mysql-community-server(5.7).

You can set Database through below DB information.

| *name* | *description* |
| --- | --- |
| DB host | 127.0.0.1 |
| DB name | confluence |
| DB user | confluenceuser |
| DB password | confluenceuser |

## Final stage
You can access http://localhost:8025 and you can find the Confluence initial page.
There is no license file. so you should register trial license in your atlassian homepage.
So, you should register your Confluence and get the trial license.